#include "scene.h"

#include <GL/glut.h>

#include <math.h>

void init_scene(Scene* scene)
{
	scene->teapot_rotation = 0.0;
}

void update_scene(Scene* scene, double time)
{
	scene->teapot_rotation += 10.0 * time;
	if(scene->teapot_rotation > 360.0){
	scene->teapot_rotation -= 360.0;
	}
}

void draw_scene(const Scene* scene)
{
	int i, k;
    draw_origin();
	draw_cylinder();
	
    glPushMatrix();
	for (k = 0; k < 8; ++k){
	     glPushMatrix();
	for (i = 0; i <6, ++i){

	  glBegin(GL_TRIANGLES);

      glColor3f(1, 0, 0);
      glVertex3f(1, 0, 0);

      glColor3f(0, 1, 0);
      glVertex3f(0, 1, 0);

      glColor3f(0, 0, 1);
      glVertex3f(0, 0, 1);

      glEnd();
	  
	  glScalef(1, 1, 1.5);
	  glTranslatef(0, 0, 0.2);
	   }
	   glPopMatrix();

	   	 glTranslatef(1, 1, 0);
		 glutWireTeapot(1);
	}
		glPopMatrix();
	
	    glPushMatrix();
		glRotatef(scene->teapot_rotation, 1, 0, 0);
		glColor3f(cos(scene->teapot_rotation / 60), sin(scene->teapot_rotation / 80), 0);
	    glutWireTeapot(scene->teapot_rotation / 360.0);
		glPopMatrix();

}

void draw_cylinder()
{
	float x, y, phi, h;
	const float MATH_PI = 3.14159265358979;
	
	glPushMatrix();
	
	glBegin(GL_QUAD_STRIP);
	
	phi = 0.0;
	h = 1.0;
	
	while (phi < MATH_PI){
		x = cos(phi);
		y = sin(phi);
		glColor3f(phi / MATH_PI, 1.0 - (phi / MATH_PI), 0);
		glVertex3f(x, y, 0);
		glVertex3f(x, y, h);
		phi += 0.01;
	}

    glEnd();
	
	glPopMatrix();
}

void draw_origin()
{
    glBegin(GL_LINES);

    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(1, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 1, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 1);

    glEnd();
}
